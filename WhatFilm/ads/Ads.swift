//
//  Ads.swift
//  Tuibady
//
//  Created by Ayoub BOUGSID on 5/22/19.
//  Copyright © 2019 John Lui. All rights reserved.
//

import Foundation
import UIKit

class Ads {
    static let config = RemoteConfig.instance
    
    static let network = config.get(key : RemoteConfig.network)
    static let banner = config.get(key : RemoteConfig.banner)
    static let interstitial = config.get(key : RemoteConfig.interstitial)
    static let showAdEveryNTime = config.get(key : RemoteConfig.showAdEveryNTime)
    
    var adNetwork: AdsProtocol!
    
    public init(viewController : UIViewController, view : UIView, withBanner : Bool = false) {
        if(Ads.network != nil){
            self.adNetwork = AdsFactory.getNetwork(network: Ads.network!, viewController: viewController, view: view,withBanner: withBanner)
        }
    }
    
    func displayInterstitialAd(){
        if(adNetwork != nil){
           self.adNetwork.displayInterstitialAd()
        }
    }
    
    func displayInterstitialAd(onDismissed: @escaping () -> Void){
        if(adNetwork != nil){
            self.adNetwork.displayInterstitialAd(onDismissed : onDismissed)
        }else{
            onDismissed()
        }
    }

}

protocol AdsProtocol {
    func displayInterstitialAd()
    func displayInterstitialAd(onDismissed: @escaping () -> Void)
}

struct AdsFactory{
    static func getNetwork(network : String, viewController : UIViewController, view : UIView, withBanner : Bool )->AdsProtocol {
        switch network {
        case "facebook":
            return FBAds(viewController: viewController, view: view,withBanner: withBanner, banner: Ads.banner, interstitial: Ads.interstitial, showAdEveryNTime: Ads.showAdEveryNTime)
        default:
            return GAds(viewController: viewController, view: view,withBanner: withBanner, banner: Ads.banner, interstitial: Ads.interstitial, showAdEveryNTime: Ads.showAdEveryNTime)
        }
    }
}
