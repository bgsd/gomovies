//
//  GAds.swift
//  Tuibady
//
//  Created by Ayoub BOUGSID on 5/22/19.
//  Copyright © 2019 John Lui. All rights reserved.
//

import Foundation
import GoogleMobileAds
class GAds: NSObject , GADInterstitialDelegate, GADBannerViewDelegate, AdsProtocol{
    var banner: String!
    var interstitial: String!
    var showAdEveryNTime: Int!
    var bannerAd: GADBannerView!
    var interstitialAd: GADInterstitial!
    var onInterstitialDismissed : (() -> Void)? = nil
    
    var viewController : UIViewController!
    var view : UIView!
    var adDisplayedNTime = 0
    
    
    public init(viewController : UIViewController, view : UIView, withBanner : Bool = false, banner: String?, interstitial: String?, showAdEveryNTime: String?) {
        super.init()
        GADMobileAds.init()
        self.viewController = viewController
        self.view = view
        self.banner = banner
        self.interstitial = interstitial
        if(showAdEveryNTime != nil){
            self.showAdEveryNTime = Int(showAdEveryNTime!)
        }else{
            self.showAdEveryNTime = 1
        }
        
        if(self.interstitial != nil){
            createAndLoadInterstitial()
        }
        
        if(withBanner && self.banner != nil){
            initAndLoadBanner()
        }
    }
    
    func initAndLoadBanner(){
        bannerAd = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        self.addBannerViewToView(bannerAd, view)
        bannerAd.adUnitID = self.banner
        bannerAd.delegate = self
        bannerAd.rootViewController = self.viewController
        bannerAd.load(GADRequest())
    }
    
    private func addBannerViewToView(_ bannerView: GADBannerView,_ view: UIView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: viewController.bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    private func createAndLoadInterstitial() {
        interstitialAd = GADInterstitial(adUnitID: self.interstitial)
        interstitialAd.delegate = self
        interstitialAd.load(GADRequest())
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        if(self.onInterstitialDismissed != nil){
            self.onInterstitialDismissed!()
        }
        createAndLoadInterstitial()
        
    }
    
    
    func displayInterstitialAd(){
        self.onInterstitialDismissed = nil
        if(self.interstitialAd != nil && adDisplayedNTime % self.showAdEveryNTime == 0){
            if interstitialAd.isReady {
                interstitialAd.present(fromRootViewController: self.viewController)
            } else {
                print("Ad wasn't ready")
            }
        }
        adDisplayedNTime+=1
    }
    
    func displayInterstitialAd(onDismissed: @escaping () -> Void) {
        self.onInterstitialDismissed = onDismissed
        if(self.interstitialAd != nil && adDisplayedNTime % self.showAdEveryNTime == 0){
            if interstitialAd.isReady {
                interstitialAd.present(fromRootViewController: self.viewController)
            } else {
                print("Ad wasn't ready")
                onDismissed()
            }
        }else{
            onDismissed()
        }
        adDisplayedNTime+=1
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}
