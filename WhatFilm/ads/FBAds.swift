//
//  GAds.swift
//  Tuibady
//
//  Created by Ayoub BOUGSID on 5/22/19.
//  Copyright © 2019 John Lui. All rights reserved.
//

import Foundation
import FBAudienceNetwork

class FBAds: NSObject , FBInterstitialAdDelegate, FBAdViewDelegate, AdsProtocol{
    var banner: String!
    var interstitial: String!
    var showAdEveryNTime: Int!
    var bannerAd: FBAdView!
    var interstitialAd: FBInterstitialAd!
    var onInterstitialDismissed : (() -> Void)? = nil

    var viewController : UIViewController!
    var view : UIView!
    var adDisplayedNTime = 0
    
    

    
    public init(viewController : UIViewController, view : UIView, withBanner : Bool = false, banner: String?, interstitial: String?, showAdEveryNTime: String?) {
        super.init()
        self.viewController = viewController
        self.view = view
        self.banner = banner
        self.interstitial = interstitial
        if(showAdEveryNTime != nil){
            self.showAdEveryNTime = Int(showAdEveryNTime!)
        }else{
            self.showAdEveryNTime = 1
        }
    
        if(self.interstitial != nil){
            createAndLoadInterstitial()
        }
        
        if(withBanner && self.banner != nil){
            initAndLoadBanner()
        }
        
    }
    
    func initAndLoadBanner(){
        bannerAd = FBAdView(placementID: self.banner, adSize: kFBAdSizeHeight50Banner, rootViewController: viewController)
        self.addBannerViewToView(bannerAd, view)
        bannerAd.delegate = self
        bannerAd.loadAd()
    }
    
    private func addBannerViewToView(_ bannerView: FBAdView,_ view: UIView) {
        bannerView.frame = CGRect(x: 0, y: view.frame.size.height - 100 , width: self.view.frame.width, height: 50)
        view.addSubview(bannerView)
    }
    
    private func createAndLoadInterstitial() {
        interstitialAd = FBInterstitialAd(placementID: self.interstitial)
        interstitialAd.delegate = self
        interstitialAd.load()
    }
    
    func interstitialAdWillClose(_ ad: FBInterstitialAd) {
        if(self.onInterstitialDismissed != nil){
            self.onInterstitialDismissed!()
        }
        createAndLoadInterstitial()
        
    }
    
    /*func didFailWithError(_ ad: FBInterstitialAd, error: NSError) {
        print("Faild to Load FBA Interstitial")
    }*/
    
    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
        print("failed")
        print(error.localizedDescription)
    }
    
    func adView(_ adView: FBAdView, didFailWithError error: Error) {
        print("failed")
        print(error.localizedDescription)
    }
    
    func displayInterstitialAd(){
        self.onInterstitialDismissed = nil
        if(self.interstitialAd != nil && adDisplayedNTime % self.showAdEveryNTime == 0){
            if interstitialAd.isAdValid {
                interstitialAd.show(fromRootViewController: viewController)
            } else {
                print("Facebook Ad wasn't valid")
            }
        }
        adDisplayedNTime+=1
    }
    
    func displayInterstitialAd(onDismissed: @escaping () -> Void) {
        self.onInterstitialDismissed = onDismissed
        if(self.interstitialAd != nil && adDisplayedNTime % self.showAdEveryNTime == 0){
            if interstitialAd.isAdValid {
                interstitialAd.show(fromRootViewController: viewController)
            } else {
                print("Ad wasn't ready")
                onDismissed()
            }
        }else{
            onDismissed()
        }
        adDisplayedNTime+=1
    }
    
}
