import Foundation
import GoogleMobileAds
import SystemConfiguration

class RemoteConfig {
    
    static let network = "network"
    static let banner = "banner"
    static let interstitial = "interstitial"
    static let showAdEveryNTime = "showAdEveryNTime"
    
    static var instance = RemoteConfig()
    var app: App!
    
    let dummy: [String: String] = [
        "network": "facebook",
        "banner": "309720496361889_325260898141182",
        "interstitial": "309720496361889_325261744807764",
        "showAdEveryNTime": "1",
        
    ]
    
    init() {
        if(RemoteConfig.isConnectedToNetwork()){
            self.fetchConfig()
        }
    }
    
    func fetchConfig() {
        let bundleIdentifier = Bundle.main.bundleIdentifier
        let urlString = "https://iosapps.host/api/apps/\(bundleIdentifier ?? "default.app")"
        guard let url = URL(string: urlString) else { return }
        let semaphore = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let data = data else { return }
            do {
                let app = try JSONDecoder().decode(App.self, from: data)
                self.app = app
                semaphore.signal()
                
            } catch let jsonError {
                print("------------------")
                print(jsonError)
            }
            }.resume()
        semaphore.wait()
    }
    
    //This is onmy for this very specific app, please don't use it on another app
    func getClientId() -> String{
        if(self.app.client_id == nil){
            return ""
        }
        return self.app.client_id!
    }
    //---------------------------------------------------------------------------
    func get(key : String) -> String? {
        if(app == nil){
            return nil
        }
        switch(key){
            case RemoteConfig.network :
                return app.network
            case RemoteConfig.banner :
                return app.banner
            case RemoteConfig.interstitial :
                return app.interstitial
            case RemoteConfig.showAdEveryNTime :
                return app.showAdEveryNTime
            default:
                return ""
        }
    }
    
    static func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }

        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    
}

struct App:Decodable {
    let network: String?
    let banner: String?
    let interstitial: String?
    let showAdEveryNTime: String?
    let client_id: String?
    
    enum CodingKeys: String, CodingKey {
        case network = "ad_network"
        case banner = "banner"
        case interstitial = "interstitial"
        case showAdEveryNTime = "interstitial_clicks"
        case client_id = "onesignal_app_id"
    }
}
