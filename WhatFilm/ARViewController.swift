//
//  ARViewController.swift
//  GoMovies
//
//  Created by Ayoub BOUGSID on 1/21/19.
//  Copyright © 2019 Julien Ducret. All rights reserved.
//

import Foundation
import UIKit
import ARKit
import YoutubeDirectLinkExtractor

class ARViewController: UIViewController,ARSCNViewDelegate{
    @IBAction func done(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "tab")
        self.present(controller, animated: true, completion: nil)
        
    }
    var video: String!
    @IBOutlet var sceneView: ARSCNView!
    let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
    struct AspectRatio {
        static let width: CGFloat = 320
        static let height: CGFloat = 240
    }
    let AspectDiv: CGFloat = 1000
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var player = AVPlayer(url: videoURL!)
        let y = YoutubeDirectLinkExtractor()
        y.extractInfo(for: .urlString("https://www.youtube.com/watch?v="+self.video), success: { info in
            if(info.highestQualityPlayableLink==nil){
                player = AVPlayer(url: self.videoURL!)
            }else{
                player = AVPlayer(url: URL(string: info.highestQualityPlayableLink!)!)
            }
            // create AVPlayer
            
            // place AVPlayer on SKVideoNode
            let playerNode = SKVideoNode(avPlayer: player)
            // flip video upside down
            playerNode.yScale = -1
            
            // create SKScene and set player node on it
            let spriteKitScene = SKScene(size: CGSize(width: AspectRatio.width, height: AspectRatio.height))
            spriteKitScene.scaleMode = .aspectFit
            playerNode.position = CGPoint(x: spriteKitScene.size.width/2, y: spriteKitScene.size.height/2)
            playerNode.size = spriteKitScene.size
            spriteKitScene.addChild(playerNode)
            
            // create 3D SCNNode and set SKScene as a material
            let videoNode = SCNNode()
            videoNode.geometry = SCNPlane(width: 0.2, height: 0.1)
            videoNode.geometry?.firstMaterial?.diffuse.contents = spriteKitScene
            videoNode.geometry?.firstMaterial?.isDoubleSided = true
            // place SCNNode inside ARKit 3D coordinate space
            videoNode.position = SCNVector3(x: 0, y: 0, z: -0.5)
            
            // create a new scene
            let scene = SCNScene()
            scene.rootNode.addChildNode(videoNode)
            // set the scene to the view
            self.sceneView.scene = scene
            playerNode.play()
        }) { error in
            print(error)
        }
        
    }
    
}
