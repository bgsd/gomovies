//
//  Settings.swift
//  WhatFilm
//
//  Created by Julien Ducret on 28/09/2016.
//  Copyright © 2016 Julien Ducret. All rights reserved.
//

import UIKit

public struct Settings {
    
    // MARK: - Private initializer

    private init() {}
    
    // MARK: - Functions
    
    static func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("Font Names = [\(names)]")
        }
    }
    
    static func initializeServices() {
        TMDbAPI.instance.start()
    }
    
    static func setupAppearance() {
        
        // Tint colors
        UIApplication.shared.delegate?.window??.tintColor = UIColor(commonColor: .yellow)
        UIRefreshControl.appearance().tintColor = UIColor(commonColor: .yellow)
        UITabBar.appearance().barTintColor = UIColor(commonColor: .black)
        
        // Global font
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.font: TextStyle.navigationTitle.font,NSAttributedStringKey.foregroundColor:UIColor.white]
        UILabel.appearance().font = TextStyle.body.font
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: TextStyle.body.font,NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        UINavigationBar.appearance().barTintColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        
        // UINavigation bar
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = true
    }
}
