//
//  FeaturedViewController.swift
//  WhatFilm
//
//  Created by Julien Ducret on 23/09/2016.
//  Copyright © 2016 Julien Ducret. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import GoogleMobileAds
import AdSupport

final class FeaturedViewController: BaseFilmCollectionViewController, ReactiveDisposable {
    
    
    
    
    @IBOutlet var topView: UIView!
    
    @IBOutlet weak var adContainer: UIView!
    
    fileprivate let refreshControl: UIRefreshControl = UIRefreshControl()
    
    // MARK: - Properties
    
    fileprivate let keyboardObserver: KeyboardObserver = KeyboardObserver()
    fileprivate let viewModel: FeaturedViewModel = FeaturedViewModel()
    let disposeBag: DisposeBag = DisposeBag()
    var adHelper : Ads!
    // MARK: - UIViewController life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.setupCollectionView()
        self.setupBindings()
        self.viewModel.reloadTrigger.onNext(())
        self.adHelper = Ads(viewController: self,view: view,withBanner: true)
    }
    
    func identifierForAdvertising() -> String? {
        // Check whether advertising tracking is enabled
        guard ASIdentifierManager.shared().isAdvertisingTrackingEnabled else {
            return nil
        }
        
        // Get and return IDFA
        return ASIdentifierManager.shared().advertisingIdentifier.uuidString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("---------")
        print(identifierForAdvertising())

    }
    
    // MARK: - Reactive bindings setup
    
    fileprivate func setupBindings() {
        
        // Bind refresh control to data reload
        self.refreshControl.rx
            .controlEvent(.valueChanged)
            .filter({ self.refreshControl.isRefreshing })
            .bindTo(self.viewModel.reloadTrigger)
            .addDisposableTo(self.disposeBag)
        
        // Bind view model films to the table view
        self.viewModel
            .films
            .bindTo(self.collectionView.rx.items(cellIdentifier: FilmCollectionViewCell.DefaultReuseIdentifier, cellType: FilmCollectionViewCell.self)) {
                (row, film, cell) in
                cell.populate(withPosterPath: film.posterPath, andTitle: film.fullTitle)
            }.addDisposableTo(self.disposeBag)
        
        // Bind view model films to the refresh control
        self.viewModel.films
            .subscribe { _ in
                self.refreshControl.endRefreshing()
            }.addDisposableTo(self.disposeBag)
        
        // Bind table view bottom reached event to loading the next page
        self.collectionView.rx
            .reachedBottom
            .bindTo(self.viewModel.nextPageTrigger)
            .addDisposableTo(self.disposeBag)
        
        // Bind keyboard updates to table view inset
        self.keyboardObserver
            .willShow
            .subscribe(onNext: { [unowned self] (keyboardInfo) in
                self.setupScrollViewViewInset(forBottom: keyboardInfo.frameEnd.height, animationDuration: keyboardInfo.animationDuration)
            }).addDisposableTo(self.disposeBag)
        self.keyboardObserver
            .willHide
            .subscribe(onNext: { [unowned self] (keyboardInfo) in
                self.setupScrollViewViewInset(forBottom: 0, animationDuration: keyboardInfo.animationDuration)
            }).addDisposableTo(self.disposeBag)
    }
    
    // MARK: - UI Setup
    
    fileprivate func setupUI() {
        //topView.bringSubview(toFront: bannerAdView)
    }
    
    fileprivate func setupCollectionView() {
        self.collectionView.registerReusableCell(FilmCollectionViewCell.self)
        self.collectionView.rx.setDelegate(self).addDisposableTo(self.disposeBag)
        self.collectionView.addSubview(self.refreshControl)
    }
    
    fileprivate func setupScrollViewViewInset(forBottom bottom: CGFloat, animationDuration duration: Double? = nil) {
        let inset = UIEdgeInsets(top: 0, left: 0, bottom: bottom, right: 0)
        if let duration = duration {
            UIView.animate(withDuration: duration, animations: {
                self.collectionView.contentInset = inset
                self.collectionView.scrollIndicatorInsets = inset
            })
        } else {
            self.collectionView.contentInset = inset
            self.collectionView.scrollIndicatorInsets = inset
        }
    }
    
    // MARK: - Navigation handling
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let filmDetailsViewController = segue.destination as? FilmDetailsViewController,
            let PushFilmDetailsSegue = segue as? PushFilmDetailsSegue,
            let indexPath = sender as? IndexPath,
            let cell = self.collectionView.cellForItem(at: indexPath) as? FilmCollectionViewCell {
            do {
                let film: Film = try collectionView.rx.model(at: indexPath)
                self.preparePushTransition(to: filmDetailsViewController, with: film, fromCell: cell, via: PushFilmDetailsSegue)
                self.adHelper.displayInterstitialAd()
            } catch { fatalError(error.localizedDescription) }
        }
    }
}

// MARK: -

extension FeaturedViewController: UITableViewDelegate {
    
    // MARK: - UITableViewDelegate functions
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.adHelper.displayInterstitialAd(onDismissed: {
            self.onMoviesClicked(indexPath: indexPath)
        })
        

        
    }
    
    typealias onMoviesClickedHandler = (_ indexPath: IndexPath)  -> Void
    
    func onMoviesClicked(indexPath: IndexPath){
        do {
            let film: Film = try collectionView.rx.model(at: indexPath)
            let viewModel: FilmDetailsViewModel = FilmDetailsViewModel(withFilmId: film.id)
            let alert = UIAlertController(title: "Options", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Play on Augmented Reality", style: UIAlertActionStyle.destructive, handler: { action in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ARViewController") as! ARViewController
                viewModel
                    .filmDetail
                    .subscribe({ [weak self] (filmDetail) in
                        let videos = filmDetail.element?.videos;
                        if(videos == nil) {
                            return;
                        }
                        if(videos!.count>0){
                            controller.video = videos?[0].key
                            self?.present(controller, animated: true, completion: nil)
                        }
                    }).addDisposableTo(self.disposeBag)
            }))
            alert.addAction(UIAlertAction(title: "Movie Details", style: UIAlertActionStyle.destructive, handler: { action in
                self.performSegue(withIdentifier: FilmDetailsViewController.segueIdentifier, sender: indexPath)
            }))
            alert.addAction(UIAlertAction(title: "Get on iTunes", style: UIAlertActionStyle.destructive, handler: { action in
                let semaphore = DispatchSemaphore(value: 0)
                let urlString = "https://itunes.apple.com/search?term="+film.title+"&entity=movie"
                let gitUrl = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
                URLSession.shared.dataTask(with: gitUrl!) { (data, response, error) in guard let data = data else { return }
                    do {
                        let decoder = JSONDecoder()
                        let itunesResponse = try decoder.decode(ItunesResponse.self, from: data)
                        semaphore.signal()
                        if(itunesResponse.results?.count != 0){
                            let trackId = itunesResponse.results?.first?.trackId
                            UIApplication.shared.openURL(URL(string: "itms://itunes.apple.com/us/movie/id"+String(trackId!))!)
                        }else{
                            let a = UIAlertController(title: "Item Not Available", message: "", preferredStyle: UIAlertControllerStyle.alert)
                            a.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                            }))
                            self.present(a, animated: true, completion: nil)
                            
                        }
                    } catch let err {
                        print("Err", err)
                    }
                    }.resume()
                semaphore.wait()
                
            }))
            
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = UIViewController()
            window.makeKeyAndVisible()
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: false, completion: nil)
            

            //self.present(alert, animated: true, completion: nil)
        } catch { fatalError(error.localizedDescription) }
    }
}

extension FeaturedViewController: FilmDetailsFromCellTransitionable { }
