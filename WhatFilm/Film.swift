//
//  Film.swift
//  WhatFilm
//
//  Created by Julien Ducret on 12/09/2016.
//  Copyright © 2016 Julien Ducret. All rights reserved.
//

import UIKit
import SwiftyJSON
import GRDB

public class Film: Record {

    public override static var databaseTableName: String { return "films" }
    let id: Int
    let posterPathString: String?
    let adult: Bool
    let overview: String
    let releaseDate: Date
    let genreIds: [Int]
    let originalTitle: String
    let originalLanguage: String
    let title: String
    let backdropPathString: String?
    let popularity: Double
    let voteCount: Int
    let video: Bool
    let voteAverage: Double
    var favourite : Bool
    
    // MARK: - Computed properties
    
    var fullTitle: String {
        let date = self.releaseDate as NSDate
        return self.title + " (\(date.year()))"
    }
    
    var posterPath: ImagePath? {
        guard let posterPathString = self.posterPathString else { return nil }
        return ImagePath.poster(path: posterPathString)
    }
    
    var backdropPath: ImagePath? {
        guard let backdropPathString = self.backdropPathString else { return nil }
        return ImagePath.backdrop(path: backdropPathString)
    }
    
    // MARK: - JSONInitializable initializer
    
    public required init(json: JSON) {
        self.id = json["id"].intValue
        self.posterPathString = json["poster_path"].string
        self.adult = json["adult"].boolValue
        self.overview = json["overview"].stringValue
        self.releaseDate = json["release_date"].dateValue
        self.genreIds = json["genre_ids"].arrayValue.flatMap({ $0.int })
        self.originalTitle = json["original_title"].stringValue
        self.originalLanguage = json["original_language"].stringValue
        self.title = json["title"].stringValue
        self.backdropPathString = json["backdrop_path"].string
        self.popularity = json["popularity"].doubleValue
        self.voteCount = json["popularity"].intValue
        self.video = json["video"].boolValue
        self.voteAverage = json["vote_average"].doubleValue
        self.favourite = false
        super.init()
    }
    
    required public init(row: Row) {
        self.id = row["id"]
        self.posterPathString = row["posterPathString"]
        self.adult = row["adult"]
        self.overview = row["overview"]
        self.releaseDate = row["releaseDate"]
        self.originalTitle = row["originalTitle"]
        self.originalLanguage = row["originalLanguage"]
        self.title = row["title"]
        self.backdropPathString = row["backdropPathString"]
        self.popularity = row["popularity"]
        self.voteCount = row["voteCount"]
        self.video = row["video"]
        self.voteAverage = row["voteAverage"]
        self.genreIds = []
        self.favourite = row["favourite"]
        super.init()
    }
    
    public override func encode(to container: inout PersistenceContainer) {
        container["id"] = id
        container["posterPathString"] = posterPathString
        container["adult"] = adult
        container["overview"] = overview
        container["releaseDate"] = releaseDate
        container["originalTitle"] = originalTitle
        container["originalLanguage"] = originalLanguage
        container["title"] = title
        container["backdropPathString"] = backdropPathString
        container["popularity"] = popularity
        container["voteCount"] = voteCount
        container["video"] = video
        container["voteAverage"] = voteAverage
        container["favourite"] = favourite
    }
    
}

// MARK: -

extension Film {
    
    // MARK: - Description
    
    public  var description: String {
        let dateString: String = DateManager.SharedFormatter.string(from: self.releaseDate)
        return "\(self.originalTitle) (\(dateString))"
    }
    
    public  var debugDescription: String { return self.description }
}

// MARK: -

extension Array where Element: Film {
    
    var withoutDuplicates: [Film] {
        var exists: [Int: Bool] = [:]
        return self.filter { exists.updateValue(true, forKey: $0.id) == nil }
    }
}
