import GRDB

/// A type responsible for initializing the application database.
///
/// See AppDelegate.setupDatabase()
struct AppDatabase {
    /// Creates a fully initialized database at path
    static var dbQueue: DatabaseQueue!
    
    static func openDatabase(atPath path: String) throws -> DatabaseQueue {
        dbQueue = try DatabaseQueue(path: path)
        try migrator.migrate(dbQueue)
        return dbQueue
    }
    
    static var migrator: DatabaseMigrator {
        var migrator = DatabaseMigrator()
        
        migrator.registerMigration("createFilms") { db in
            try db.create(table: "films") { t in
                t.column("id", .integer).primaryKey()
                t.column("posterPathString", .text)
                t.column("adult", .boolean)
                t.column("overview", .text)
                t.column("releaseDate", .date)
                t.column("originalTitle", .text)
                t.column("originalLanguage", .text)
                t.column("title", .text)
                t.column("backdropPathString", .text)
                t.column("popularity", .double)
                t.column("voteCount", .integer)
                t.column("video", .boolean)
                t.column("voteAverage", .double)
                t.column("favourite", .boolean)
            }
        }
        
        migrator.registerMigration("fixtures") { db in
        }
        return migrator
    }
}
